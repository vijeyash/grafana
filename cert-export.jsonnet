local grafana = import 'grafonnet/grafana.libsonnet';
local dashboard = grafana.dashboard;
local prometheus = grafana.prometheus;
local graph = grafana.graphPanel;
local table = grafana.tablePanel;
local singlestat = grafana.singlestat;
local tablePanel = grafana.tablePanel;
local template = grafana.template;


dashboard.new(
  'cert-exporter',
  time_from='now-6h',
  description="Dashboard for tracking PKI certificate expirations.",
  editable=true
)
.addTemplate(
  grafana.template.datasource(
    name='datasource',
    query='prometheus',
    current='',
  )
)
.addPanel(
  singlestat.new(
    'Expiration within half year',
    colorBackground=true,
    colorValue=true,
    valueName='last'
  )
  .addTarget(
    prometheus.target(
      "count(cert_exporter_cert_expires_in_seconds <= 15724800)+count(cert_exporter_kubeconfig_expires_in_seconds <= 15724800)+count(cert_exporter_secret_expires_in_seconds <= 15724800) OR on() vector(0)",
      intervalFactor=1,
      format='time_series'
    )
  ), gridPos={ x: 0, y: 0, w: 8, h: 2,}
)
.addPanel(
  singlestat.new(
    'Expiration within half and one year',
    colorBackground=true,
    colorValue=true,
    valueName='last'
  )
  .addTarget(
    prometheus.target(
      "count(15724800 < cert_exporter_cert_expires_in_seconds and cert_exporter_cert_expires_in_seconds <= 31536000)+count(15724800 < cert_exporter_kubeconfig_expires_in_seconds and cert_exporter_kubeconfig_expires_in_seconds <= 31536000)+count(15724800 < cert_exporter_secret_expires_in_seconds and cert_exporter_secret_expires_in_seconds <= 31536000) OR on() vector(0)",
      intervalFactor=1,
      format='time_series'
    )
  ), gridPos={ x: 8, y: 0, w: 8, h: 2,}
)
.addPanel(
  singlestat.new(
    'Expiration after one year',
    colorBackground=true,
    colorValue=true,
    valueName='last'
  )
  .addTarget(
    prometheus.target(
      "count(cert_exporter_cert_expires_in_seconds > 31536000)+count(cert_exporter_kubeconfig_expires_in_seconds > 31536000)+count(cert_exporter_secret_expires_in_seconds > 31536000) OR on() vector(0)",
      intervalFactor=1,
      format='time_series'
    )
  ), gridPos={ x: 16, y: 0, w: 8, h: 2,}
)
.addPanel(
  singlestat.new(
    'Errors Total',
    colorBackground=true,
    colorValue=false,
    interval='5m',
    thresholds='1,1',
  )
  .addTarget(
    prometheus.target(
      "sum(cert_exporter_error_total)",
      intervalFactor=1,
      format='time_series'
    )
  ), gridPos={ x: 0, y: 2, w: 5, h: 6,}
)
.addPanel(
  graph.new(
    'Errors [1h]',
    interval='15s',
    legend_alignAsTable=true,
    legend_rightSide=true,
    pointradius=2,
  )
  .addTarget(
    prometheus.target(
      "increase(cert_exporter_error_total[1h])",
      intervalFactor=1,
      legendFormat= "{{kubernetes_pod_name}}",
      format='time_series'
    )
  ), gridPos={ x: 5, y: 2, w: 19, h: 6,}
)
.addPanel(
  tablePanel.new(
    title='Kubernetes and Etcd PKI Cert Expiry',
    transform='table',
    sort=[{
      col: 10,
      desc: false
    },],
    styles=[
      {
        alias: 'Time',
        align: 'auto',
        dateFormat: 'YYYY-MM-DD HH:mm:ss',
        pattern: '/(__name__|Time|controller_revision_hash|instance|job|kubernetes_namespace|pod_template_generation)/',
        type: 'hidden',
        link: false,
      },
      {
        alias: 'Expiration',
        align: 'auto',
        colorMode: 'cell',
        colors: [ "rgba(245, 54, 54, 0.9)", "rgba(237, 129, 40, 0.89)", "rgba(50, 172, 45, 0.97)" ],
        dateFormat: 'YYYY-MM-DD',
        decimals: 2,
        pattern: 'Value',
        thresholds: ["15724800", "31536000"],
        type: 'number',
        unit: 'dtdurations'
      },
    ],
  )
  .addTarget(
    prometheus.target(
      "cert_exporter_cert_expires_in_seconds",
      intervalFactor=1,
      format='table',
      instant=true
    )
  ),gridPos={ x: 0, y: 8, w: 24, h: 8,}
)
.addPanel(
  tablePanel.new(
    title='Kubeconfig Cert Expiry',
    transform='table',
    sort=[{
      col: 12,
      desc: false
    },],
    styles=[
      {
        alias: 'Time',
        align: 'auto',
        dateFormat: 'YYYY-MM-DD HH:mm:ss',
        pattern: '/(__name__|Time|controller_revision_hash|instance|job|kubernetes_namespace|pod_template_generation)/',
        type: 'hidden',
        link: false
      },
      {
        alias: 'Expiration',
        align: 'auto',
        colorMode: 'cell',
        colors: [ "rgba(245, 54, 54, 0.9)", "rgba(237, 129, 40, 0.89)", "rgba(50, 172, 45, 0.97)" ],
        dateFormat: 'YYYY-MM-DD',
        decimals: 2,
        pattern: 'Value',
        thresholds: ["15724800", "31536000"],
        type: 'number',
        unit: 'dtdurations'
      },
    ],
  )
  .addTarget(
    prometheus.target(
      "cert_exporter_kubeconfig_expires_in_seconds",
      format='table',
      intervalFactor=1,
      instant=true
    )
  ),gridPos={ x: 0, y: 16, w: 24, h: 8,}
)
